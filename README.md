# Sklep internetowy "CinaMon"

Sklep internetowy o nazwie "CinaMon" jest aplikacja, za pomoca kt�rej mo�na dokona� zakupu ubra� z dzia�u damskiego. Dla potrzeb aplikacji za�o�ono, �e wszystkie ubrania i buty maja jeden rozmiar i nie mo�na go wybiera�. W przysz�oci zostanie to ulepszone.

Aplikacja zosta�a zaimplementowana w tehnologii ASP .NET MVC 5 oraz Entity Framework. Wykorzystano r�wnie� bibliotek� Bootstrap, kt�ra umo�liwi�a dostosowanie widok�w u�ytkownika. U�yto tak�e pakietu AutoMapper w celu zmapowania modeli na obiekty typu view model.


### U�ytkownicy
W aplikacji mo�na wyr�ni� dwa rodzaje u�ytkownik�w: administrator i zwyk�y klient sklepu. Rola Administrator pozwala na:

* dodawanie, edycj� i usuwanie produkt�w i kategorii
* podglad listy u�ytkownik�w
* podglad listy zam�wie�
* edycj� has�a.

Administrator po zalogowaniu si� ma dost�p do innego widoku ni� zwyk�y u�ytkownik (widok ogranicza si� do funkcji, kt�re sa wpisane w rol� administratora). Dane do panelu administartora:

Login: _admin@example.com_

Has�o: _zaq1@WSX_

Administrator nie mo�e przy tym:

* dokonywa� zakup�w (funkcja koszyka i zam�wienia)
* edytowa� adresu.

Zas zwyk�y u�ytkownik b�dacy klientem sklepu mo�e przeglada� list� produkt�w poprzez wybranie odpowiedniej kategorii, co nie wymaga zalogowania.
Kolejne funkcjonalnosci dost�pne sa tylko dla zarejestrowanych i zalogowanych u�ytkownik�w. Nale�a do nich:

* dodawanie produkt�w do koszyka, zwi�kszanie ich ilosci oraz usuwanie z koszyka
* przejscie do zam�wienia, w kt�rym dost�pny jest podglad dodanych produkt�w oraz mo�liwos� edycji swoich danych osobowych do wysy�ki
* mo�liwos� edycji has�a
* mo�liwos� edycji adresu, kt�ry jest p�niej automatycznie pobierany do danych w zam�wieniu
* podglad dokonanych zam�wie�.

Przyk�adowe dane do logowania dla klienta sklepu to:

Login: _test@example.com_

Has�o: _zaq1@WSX_


### Informacje dodatkowe
W celu umo�liwienia przetestowania aplikacji, dodano przyk�adowe produkty oraz zrealizowane zam�wienia.

Znajduja sie one w kategoriach: _Buty_, _Koszule_, _P�aszcze_, _Spodnie_, _Swetry_, _Torebki_.