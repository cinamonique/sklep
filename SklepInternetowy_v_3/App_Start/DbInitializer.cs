﻿using System;
using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SklepInternetowy_v_3.Helpers;
using SklepInternetowy_v_3.Models;

namespace SklepInternetowy_v_3.App_Start
{
    public class DbInitializer: CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            InitializeAdminUser(context);
            base.Seed(context);
        }

        private void InitializeAdminUser(ApplicationDbContext context)
        {
            const string userName = "admin@example.com";
            const string password = "zaq1@WSX";
            const string firstName = "admin";
            const string lastName = "admin";
            const string roleName = AppConstants.AdminRole;

            var appUserStore = new UserStore<ApplicationUser>(context);
            var appUserManager = new UserManager<ApplicationUser>(appUserStore);
            var appRoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            try
            {
                var user = context.Users.SingleOrDefault(u => u.UserName == userName);
                var role = context.Roles.SingleOrDefault(r => r.Name == roleName);

                if (role == null)
                {
                    appRoleManager.CreateAsync(new IdentityRole(roleName)).Wait();
                    role = context.Roles.SingleOrDefault(r => r.Name == roleName);
                }
                if (user == null)
                {
                    appUserManager.CreateAsync(new ApplicationUser { UserName = userName, Email = userName, FirstName = firstName, LastName = lastName }, password).Wait();
                    user = context.Users.SingleOrDefault(u => u.UserName == userName);
                }

                var userRole = user.Roles.SingleOrDefault(r => r.RoleId == role.Id);

                if (userRole == null)
                {
                    appUserManager.AddToRoleAsync(user.Id, roleName).Wait();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}