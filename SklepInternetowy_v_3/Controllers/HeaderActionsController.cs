﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using SklepInternetowy_v_3.Helpers;
using SklepInternetowy_v_3.Models;
using SklepInternetowy_v_3.Models.AdminItems;
using SklepInternetowy_v_3.Models.Category;

namespace SklepInternetowy_v_3.Controllers
{
    public class HeaderActionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Categories()
        {
            var productCategories = db.ProductCategories.Where(c => c.ParentCategoryId == null).Include(c => c.SubCategories).ToList();
            var categoryViews = AutoMapper.Mapper.Map<List<ProductCategory>, List<CategoryHeaderItemViewModel>>(productCategories);

            return PartialView("_CategoriesHeaderPartial", categoryViews);
        }


        [AuthorizeRole(Roles = AppConstants.AdminRole)]
        public ActionResult Orders()
        {
            var orders = db.Orders.OrderByDescending(o => o.OrderDate).Include(o => o.User).Include(o => o.ItemsInOrder).ToList();
            var userOrders = orders.GroupBy(o => o.User.UserName, o => o, (key, order) => new UserOrdersViewModel()
            {
                UserName = key,
                SubmittedOrders = AutoMapper.Mapper.Map<List<Order>,List<SubmittedOrderViewModel>>(order.ToList())
            });
            return View(userOrders);
        }

        [AuthorizeRole(Roles = AppConstants.AdminRole)]
        public ActionResult Users()
        {
            var users = db.Users.ToList();
            var usersNames = users.Select(u => new UserDetailsViewModel(u.UserName, u.FirstName, u.LastName));
            return View(usersNames);
        }
    }
}