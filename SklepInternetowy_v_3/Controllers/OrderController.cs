﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SklepInternetowy_v_3.Models;
using AutoMapper;

namespace SklepInternetowy_v_3.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Orders/Order
        public ActionResult Order()
        {
            string userId = User.Identity.GetUserId();
            ApplicationUser user = db.Users.SingleOrDefault(u => u.Id == userId);
            ShoppingCart shoppingCart = db.ShoppingCarts.SingleOrDefault(c => c.CartId == userId);
            if (user == null || shoppingCart == null)
            {
                return HttpNotFound();
            }
            OrderViewModel orderViewModel = new OrderViewModel()
            {
                ShoppingCartViewModel = Mapper.Map<ShoppingCart, ShoppingCartViewModel>(shoppingCart),
                AddressViewModel = Mapper.Map<Address, AddressViewModel>(user.UserAddress),
                FirstName = user.FirstName,
                LastName = user.LastName,
                Total = Mapper.Map<ShoppingCart, ShoppingCartViewModel>(shoppingCart).TotalPrizesUp
            };
            return View(orderViewModel);
        }

        //POST: Order/Order
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Order(OrderViewModel orderViewModel)
        {
            if (ModelState.IsValid)
            {
                Order order = new Order();
                order.ItemsInOrder = new List<CartItem>();
                foreach (CartItemViewModel itemInOrder in orderViewModel.ShoppingCartViewModel.Items)
                {
                    Product productInStock = db.Products.SingleOrDefault(p => p.ProductId == itemInOrder.ProductInCart.ProductId & p.ProductAmount > 0);
                    if (productInStock == null)
                    {
                        return OrderResult("error", "Przepraszamy. Niestety produkt został wyprzedany. Prosimy o szybsze potwierdzenie zakupu ;)");
                    }
                    if (productInStock.ProductAmount >= itemInOrder.Quantity)
                    {
                        order.ItemsInOrder.Add(new CartItem() { ProductId = itemInOrder.ProductInCart.ProductId, Quantity = itemInOrder.Quantity });
                        productInStock.ProductAmount -= itemInOrder.Quantity;
                        db.Entry(productInStock).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else 
                    {
                        return OrderResult("error", "Przepraszamy. Niestety produkt został wyprzedany. Prosimy o szybsze potwierdzenie zakupu ;)");
                    }
                }
                string userId = User.Identity.GetUserId();
                order.UserId = userId;
                order.OrderDate = DateTime.Now;
                order.DeliveryDetails = new DeliveryDetails();
                order.DeliveryDetails.FirstName = orderViewModel.FirstName;
                order.DeliveryDetails.LastName = orderViewModel.LastName;
                order.DeliveryDetails.PhoneNumber = orderViewModel.PhoneNumber;
                order.DeliveryDetails.Street = orderViewModel.AddressViewModel.Street;
                order.DeliveryDetails.HouseNumber = orderViewModel.AddressViewModel.HouseNumber ?? default(int);
                order.DeliveryDetails.FlatNumber = orderViewModel.AddressViewModel.FlatNumber;
                order.DeliveryDetails.ZipCode = orderViewModel.AddressViewModel.ZipCode;
                order.DeliveryDetails.City = orderViewModel.AddressViewModel.City;
                db.Orders.Add(order);
                db.SaveChanges();
                ShoppingCart currentShoppingCart = db.ShoppingCarts.SingleOrDefault(c => c.CartId == userId);
                if (currentShoppingCart == null)
                {
                    return HttpNotFound();
                }
                currentShoppingCart.Items.Clear();
                db.Entry(currentShoppingCart).State = EntityState.Modified;
                db.SaveChanges();
                return OrderResult("success", "Dziękujemy.\nZamówienie przyjęte, prosimy o dokonanie płatności, aby mogło zostać zrealizowane.");
            }
            return View(orderViewModel);
        }

        private ActionResult OrderResult(string status, string message)
        {
            return View("OrderResult", new Tuple<string, string>(status, message));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}