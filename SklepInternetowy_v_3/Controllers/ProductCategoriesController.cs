﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SklepInternetowy_v_3.Models;
using AutoMapper;
using SklepInternetowy_v_3.Helpers;

namespace SklepInternetowy_v_3.Controllers
{
    [AuthorizeRole(Roles = AppConstants.AdminRole)]
    public class ProductCategoriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ProductCategories
        public ActionResult Index()
        {
            List<ProductCategory> categories = db.ProductCategories.ToList();
            var categoriesView = Mapper.Map<List<ProductCategory>, List<ProductCategoryViewModel>>(categories);
            return View(categoriesView);
        }

        // GET: ProductCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductCategory productCategory = db.ProductCategories.Find(id);
            if (productCategory == null)
            {
                return HttpNotFound();
            }
            var productCategoryView = Mapper.Map<ProductCategory, ProductCategoryViewModel>(productCategory);
            return View(productCategoryView);
        }

        // GET: ProductCategories/Create
        public ActionResult Create()
        {
            InitParentCategories();
            return View();
        }

        private void InitParentCategories(int? selectedValue = null)
        {
            var parentCategories = db.ProductCategories.Where(c => c.ParentCategoryId == null);
            ViewBag.Categories = new SelectList(parentCategories, "CategoryId", "CategoryName", selectedValue);
        }

        // POST: ProductCategories/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductCategoryViewModel categoryViewModel)
        {
            if (ModelState.IsValid)
            {
                ProductCategory productCategory = new ProductCategory(categoryViewModel.CategoryName, categoryViewModel.ParentCategoryId);
                db.ProductCategories.Add(productCategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            InitParentCategories(categoryViewModel.ParentCategoryId);
            return View(categoryViewModel);
        }

        // GET: ProductCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductCategory productCategory = db.ProductCategories.Find(id);
            if (productCategory == null)
            {
                return HttpNotFound();
            }
            var productCategoryView = Mapper.Map<ProductCategory, ProductCategoryViewModel>(productCategory);
            InitParentCategories(productCategory.ParentCategoryId);
            return View(productCategoryView);
        }

        // POST: ProductCategories/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductCategoryViewModel categoryViewModel)
        {
            if (ModelState.IsValid)
            {
                ProductCategory productCategory = db.ProductCategories.SingleOrDefault(p => p.CategoryId == categoryViewModel.CategoryId);
                if (productCategory == null)
                {
                    return HttpNotFound();
                }
                productCategory.CategoryName = categoryViewModel.CategoryName;
                productCategory.ParentCategoryId = categoryViewModel.ParentCategoryId;

                db.Entry(productCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            InitParentCategories(categoryViewModel.ParentCategoryId);
            return View(categoryViewModel);
        }

        // GET: ProductCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductCategory productCategory = db.ProductCategories.Find(id);
            if (productCategory == null)
            {
                return HttpNotFound();
            }
            var productCategoryView = Mapper.Map<ProductCategory, ProductCategoryViewModel>(productCategory);
            return View(productCategoryView);
        }

        // POST: ProductCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductCategory productCategory = db.ProductCategories.Find(id);
            db.ProductCategories.Remove(productCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
