﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SklepInternetowy_v_3.Models;
using AutoMapper;
using SklepInternetowy_v_3.Helpers;

namespace SklepInternetowy_v_3.Controllers
{
    [AuthorizeRole(Roles = AppConstants.AdminRole)]
    public class ProductsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Products
        [AllowAnonymous]
        public ActionResult Category(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            List<Product> products = db.Products.Where(p => p.ProductAmount > 0 && p.ProductCategory.CategoryName.Equals(id, StringComparison.InvariantCulture))
                .Include(p => p.ProductCategory).Include(p => p.ProductPhotos).ToList();
            var productsView = Mapper.Map<List<Product>, List<ProductClientViewModel>>(products);
            var category = db.ProductCategories.SingleOrDefault(c => c.CategoryName.Equals(id, StringComparison.InvariantCulture));
            ViewBag.CategoryName = category != null ? category.CategoryName : "Kategoria nieznana";
            return View("UserProductList", productsView);
        }

        // GET: Products
        [AllowAnonymous]
        public ActionResult Index(int? id)
        {
            if (User.IsInRole(AppConstants.AdminRole))
            {
                List<Product> products = db.Products.Where(p => p.ProductAmount > 0).Include(p => p.ProductCategory).Include(p => p.ProductPhotos).ToList();
                var productsView = Mapper.Map<List<Product>, List<ProductViewModel>>(products);
                return View(productsView);
            }
            else
            {
                List<Product> products = db.Products.Where(p => p.ProductAmount > 0 && p.CategoryId == id).Include(p => p.ProductCategory).Include(p => p.ProductPhotos).ToList();
                var productsView = Mapper.Map<List<Product>, List<ProductClientViewModel>>(products);
                var category = db.ProductCategories.SingleOrDefault(c => c.CategoryId == id);
                ViewBag.CategoryName = category != null ? category.CategoryName : "Kategoria nieznana";
                return View("UserProductList", productsView);
            }
        }


        // GET: Products/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Include(p => p.ProductPhotos).SingleOrDefault(p => p.ProductId == id && p.ProductAmount > 0);
            if (product == null)
            {
                return HttpNotFound();
            }
            var productView = Mapper.Map<Product, ProductViewModel>(product);
            return View(productView);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            InitCategoriesList();
            return View();
        }

        private void InitCategoriesList(int? selectedValue = null)
        {
            var categories = db.ProductCategories.Where(c => c.ParentCategoryId != null);
            ViewBag.CategoryId = new SelectList(categories, "CategoryId", "CategoryName", selectedValue);
        }

        // POST: Products/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel productViewModel, IEnumerable<HttpPostedFileBase> upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.Any(u => u != null && u.ContentLength > 0))
                {
                    string validationError = validatePhotoUpload(upload);
                    if (validationError != null)
                    {
                        ModelState.AddModelError("ProductPhotos", validationError);
                        ViewBag.CategoryId = new SelectList(db.ProductCategories, "CategoryId", "CategoryName", productViewModel.CategoryId);
                        return View(productViewModel);
                    }
                    Product product = new Product()
                    {
                        ProductName = productViewModel.ProductName,
                        ProductDescription = productViewModel.ProductDescription,
                        Price = productViewModel.Price,
                        ProductAmount = productViewModel.ProductAmount,
                        CategoryId = productViewModel.CategoryId
                    };
                    product.ProductPhotos = new List<Photos>();
                    foreach (HttpPostedFileBase photo in upload)
                    {
                        string photoNameWithTimeStamp = AppendTimeStamp(photo.FileName);
                        string path = Path.Combine(Server.MapPath("~/Images"), photoNameWithTimeStamp);
                        photo.SaveAs(path);
                        product.ProductPhotos.Add(new Photos(photoNameWithTimeStamp));
                    }
                    db.Products.Add(product);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("ProductPhotos", "Produkt musi zawierać co najmniej jedno zdjęcie");
            }
            InitCategoriesList(productViewModel.CategoryId);
            return View(productViewModel);
        }

        private string validatePhotoUpload(IEnumerable<HttpPostedFileBase> upload)
        {
            if (upload.Count() > 5)
            {
                return "Nie można dodać więcej niż 5 zdjęć";
            }
            foreach (HttpPostedFileBase photo in upload)
            {
                if (photo.ContentLength > 6500000)
                {
                    return "Zdjęcie nie może być większe niż 6MB";
                }
            }
            return null;
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Include(p => p.ProductPhotos).SingleOrDefault(p => p.ProductId == id && p.ProductAmount > 0);
            if (product == null)
            {
                return HttpNotFound();
            }
            InitCategoriesList(product.CategoryId);
            var productView = Mapper.Map<Product, ProductViewModel>(product);
            return View(productView);
        }

        // POST: Products/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductViewModel productView, IEnumerable<HttpPostedFileBase> upload)
        {
            if (ModelState.IsValid)
            {
                Product product = db.Products.Include(p => p.ProductPhotos).SingleOrDefault(p => p.ProductId == productView.ProductId && p.ProductAmount > 0);
                if (product == null)
                {
                    return HttpNotFound();
                }
                if (upload != null && upload.Any(u => u != null && u.ContentLength > 0))
                {
                    string validationError = validatePhotoUpload(upload);
                    if (validationError != null)
                    {
                        ModelState.AddModelError("ProductPhotos", validationError);
                        ViewBag.CategoryId = new SelectList(db.ProductCategories, "CategoryId", "CategoryName", productView.CategoryId);
                        return View(productView);
                    }
                    DeleteAllPhotosFromFolder(product);
                    foreach (var photo in product.ProductPhotos.ToList())
                    {
                        db.Entry(photo).State = EntityState.Deleted;
                    }
                    db.SaveChanges();
                    product.ProductPhotos = new List<Photos>();
                    foreach (HttpPostedFileBase photo in upload)
                    {
                        string photoNameWithTimeStamp = AppendTimeStamp(photo.FileName);
                        string path = Path.Combine(Server.MapPath("~/Images"), photoNameWithTimeStamp);
                        photo.SaveAs(path);
                        product.ProductPhotos.Add(new Photos(photoNameWithTimeStamp));
                    }
                }
                product.Price = productView.Price;
                product.ProductName = productView.ProductName;
                product.ProductDescription = productView.ProductDescription;
                product.ProductAmount = productView.ProductAmount;
                product.CategoryId = productView.CategoryId;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.ProductCategories, "CategoryId", "CategoryName", productView.CategoryId);
            return View(productView);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.SingleOrDefault(p => p.ProductId == id && p.ProductAmount > 0);
            if (product == null)
            {
                return HttpNotFound();
            }
            var productView = Mapper.Map<Product, ProductViewModel>(product);
            return View(productView);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Include(p => p.ProductPhotos).SingleOrDefault(p => p.ProductId == id && p.ProductAmount > 0);
            if (product == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeleteAllPhotosFromFolder(product);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private void DeleteAllPhotosFromFolder(Product product)
        {
            foreach (Photos photo in product.ProductPhotos)
            {
                string path = Path.Combine(Server.MapPath("~/Images"), photo.PhotoName);
                System.IO.File.Delete(path);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public static string AppendTimeStamp(string fileName)
        {
            return string.Concat(
                Path.GetFileNameWithoutExtension(fileName),
                DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                Path.GetExtension(fileName)
                );
        }
    }
}
