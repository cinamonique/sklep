﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SklepInternetowy_v_3.Models;
using AutoMapper;
using Microsoft.AspNet.Identity;
using SklepInternetowy_v_3.Helpers;

namespace SklepInternetowy_v_3.Controllers
{
    [DenyRole(Roles = AppConstants.AdminRole)]
    public class ShoppingCartController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ShoppingCart/Cart
        public ActionResult Cart()
        {
            string userId = User.Identity.GetUserId();
            ShoppingCart shoppingCart = db.ShoppingCarts.SingleOrDefault(c => c.CartId == userId);
            if (shoppingCart == null)
            {
                return HttpNotFound("Cannot find cart for user: " + User.Identity.Name);
            }
            ShoppingCartViewModel shoppingCartView = Mapper.Map<ShoppingCart, ShoppingCartViewModel>(shoppingCart);
            return View(shoppingCartView);
        }

        // POST: ShoppingCart/Cart
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SuppressMessage("ReSharper", "RedundantIfElseBlock")]
        public ActionResult Cart(ShoppingCartViewModel shoppingCartViewModel)
        {
            if (ModelState.IsValid)
            {
                ShoppingCart shoppingCart = db.ShoppingCarts.SingleOrDefault(c => c.CartId == shoppingCartViewModel.CartId);
                if (shoppingCart.Items.Any())
                {
                    for (int k = 0; k < shoppingCart.Items.Count; k++)
                    {
                        CartItem item = shoppingCart.Items.ElementAt(k);

                        item.Quantity = shoppingCartViewModel.Items.SingleOrDefault(i => i.ItemId == item.ItemId).Quantity;
                        Product product = db.Products.SingleOrDefault(i => i.ProductId == item.ProductId && i.ProductAmount > 0);
                        if (product == null)
                        {
                            ModelState.AddModelError($"Items[{k}].Quantity", $"Produkt {item.Product.ProductName} nie jest już dostępny.");
                            shoppingCart.Items.Remove(item);
                            db.Entry(item).State = EntityState.Deleted;
                            db.SaveChanges();
                            return View(shoppingCartViewModel);
                        }
                        int productAmount = product.ProductAmount;
                        if (item.Quantity > productAmount)
                        {
                            ModelState.AddModelError($"Items[{k}].Quantity", $"Posiadamy tylko {productAmount} sztuk produktu.");
                            return View(shoppingCartViewModel);
                        }
                    }
                    if (Request.Form["shopping"] != null)
                    {
                        db.SaveChanges();
                        return RedirectToAction("Index", "Home");
                    }
                    else if (Request.Form["order"] != null)
                    {
                        db.SaveChanges();
                        return RedirectToAction("Order", "Order");
                    }
                }
            }
            return View(shoppingCartViewModel);
        }

        // GET: ShoppingCart/AddProduct/5
        [HttpPost]
        public JsonResult AddProduct(int? id)
        {
            if (id == null)
            {
                return Json(new {status ="Error", message = "Nie podano produktu do dodania" });
            }
            Product product = db.Products.Include(p => p.ProductPhotos).SingleOrDefault(p => p.ProductId == id && p.ProductAmount > 0);
            if (product == null)
            {
                return Json(new { status = "Error", message = "Niestety produkt został już wyprzedany" });
            }
            string userId = User.Identity.GetUserId();
            ShoppingCart shoppingCart = db.ShoppingCarts.SingleOrDefault(c => c.CartId == userId);
            if (shoppingCart == null)
            {
                return Json(new { status = "Error", message = "Nie znaleziono koszyka" });
            }
            if (shoppingCart.Items == null)
            {
                shoppingCart.Items = new List<CartItem>();
            }
            if (shoppingCart.Items.Any(i => i.ProductId == id))
            {
                if (shoppingCart.Items.Single(i => i.ProductId == id).Quantity >= product.ProductAmount)
                {
                    return Json(new { status = "Error", message = "Brak wystarczającej ilości produktu" });
                }
                shoppingCart.Items.Single(i => i.ProductId == id).Quantity++;
            }
            else
            {
                CartItem item = new CartItem(product.ProductId, shoppingCart.CartId, 1);
                shoppingCart.Items.Add(item);
            }
            db.SaveChanges();
            return Json(new { status = "Success", message = "Produkt dodany do koszyka" });
        }

        //GET: ShoppingCart/DeleteProduct/5
        public ActionResult DeleteItem(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CartItem item = db.CartItems.SingleOrDefault(i => i.ItemId == id);
            if (item == null)
            {
                return HttpNotFound();
            }
            db.Entry(item).State = EntityState.Deleted;
            db.SaveChanges();
            return RedirectToAction("Cart");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}