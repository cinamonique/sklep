﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using SklepInternetowy_v_3.App_Start;
using SklepInternetowy_v_3.Models;
using SklepInternetowy_v_3.Models.Category;

namespace SklepInternetowy_v_3
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Mapper.Initialize(cfg => 
            {
                cfg.CreateMap<Product, ProductViewModel>()
                    .ForMember(dest => dest.ProductCategory, opt => opt.MapFrom(src => src.ProductCategory.CategoryName))
                    .ForMember(dest => dest.ProductPhotos, opt => opt.MapFrom(src => src.ProductPhotos.Select(photo => photo.PhotoName).ToList()));
                cfg.CreateMap<ProductCategory, ProductCategoryViewModel>()
                    .ForMember(dest => dest.ParentCategoryName, opt => opt.MapFrom(src => src.ParentCategory.CategoryName));
                cfg.CreateMap<Address, AddressViewModel>();
                cfg.CreateMap<ShoppingCart, ShoppingCartViewModel>();
                cfg.CreateMap<Product, ProductClientViewModel>()
                    .ForMember(dest => dest.ProductPhoto, opt => opt.MapFrom(src => src.ProductPhotos.First().PhotoName));
                cfg.CreateMap<Order, OrderViewModel>();
                cfg.CreateMap<CartItem, CartItemViewModel>()
                // TODO: Change this
                    .ForMember(dest => dest.ProductInCart, opt => opt.MapFrom(src => new ProductCartViewModel
                    {
                        ProductId = src.Product.ProductId,
                        ProductName = src.Product.ProductName,
                        Price = src.Product.Price,
                        ProductPhoto =  src.Product.ProductPhotos.First().PhotoName
                    }));
                cfg.CreateMap<Order, SubmittedOrderViewModel>();
                cfg.CreateMap<DeliveryDetails, DeliveryDetailsViewModel>();
                cfg.CreateMap<ProductCategory, CategoryHeaderItemViewModel>();
            });

            Database.SetInitializer(new DbInitializer());
        }
    }
}
