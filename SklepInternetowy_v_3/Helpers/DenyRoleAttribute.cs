﻿using System.Web;
using System.Web.Mvc;

namespace SklepInternetowy_v_3.Helpers
{
    public class DenyRoleAttribute: AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.User.Identity.IsAuthenticated && !base.AuthorizeCore(httpContext);
        }
    }
}