﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SklepInternetowy_v_3.Helpers
{
    public static class ImageHelper
    {
        public static MvcHtmlString Image(this HtmlHelper helper, string src, string altTetxt, string classForStyle)
        {
           var builder = new TagBuilder("img");
            //builder.MergeAttributes(new Dictionary<string, string>( {})); 
            builder.MergeAttribute("src", src);
            builder.MergeAttribute("alt", altTetxt);
            builder.MergeAttribute("class", classForStyle);
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }
    }
}