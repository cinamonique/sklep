﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models.AdminItems
{
    public class UserDetailsViewModel
    {
        [DisplayName("Nazwa użytkownika")]
        public string UserName { get; set; }

        [DisplayName("Imię")]
        public string FirstName { get; set; }

        [DisplayName("Nazwisko")]
        public string LastName { get; set; }

        public UserDetailsViewModel()
        {
        }

        public UserDetailsViewModel(string userName, string firstName, string lastName)
        {
            UserName = userName;
            FirstName = firstName;
            LastName = lastName;
        }
    }
}