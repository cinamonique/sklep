﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models.AdminItems
{
    public class UserOrdersViewModel
    {
        public string UserName { get; set; }

        public List<SubmittedOrderViewModel> SubmittedOrders { get; set; }
    }
}