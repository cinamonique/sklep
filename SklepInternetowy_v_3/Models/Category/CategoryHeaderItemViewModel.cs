﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models.Category
{
    public class CategoryHeaderItemViewModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public List<CategoryHeaderItemViewModel> SubCategories { get; set; }
    }
}