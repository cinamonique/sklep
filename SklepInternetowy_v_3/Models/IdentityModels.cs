﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SklepInternetowy_v_3.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string AddressId { get; set; }

        public string CartId { get; set; }

        public virtual Address UserAddress { get; set; }

        public virtual ShoppingCart UserCart { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<SklepInternetowy_v_3.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<SklepInternetowy_v_3.Models.ProductCategory> ProductCategories { get; set; }

        public System.Data.Entity.DbSet<SklepInternetowy_v_3.Models.Address> Addresses { get; set; }

        public System.Data.Entity.DbSet<SklepInternetowy_v_3.Models.ShoppingCart> ShoppingCarts { get; set; }

        public System.Data.Entity.DbSet<SklepInternetowy_v_3.Models.CartItem> CartItems { get; set; }

        public System.Data.Entity.DbSet<SklepInternetowy_v_3.Models.Order> Orders { get; set; }
    }
}