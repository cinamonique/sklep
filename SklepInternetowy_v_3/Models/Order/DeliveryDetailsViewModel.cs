﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class DeliveryDetailsViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string Street { get; set; }

        public int HouseNumber { get; set; }

        public int? FlatNumber { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }
    }
}