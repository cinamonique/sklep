﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class Order
    {
        [Key]
        public long OrderId { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        public List<CartItem> ItemsInOrder { get; set; }

        [Required]
        public string UserId { get; set; }

        public virtual DeliveryDetails DeliveryDetails { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}