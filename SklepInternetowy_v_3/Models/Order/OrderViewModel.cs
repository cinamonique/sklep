﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class OrderViewModel
    {
        public ShoppingCartViewModel ShoppingCartViewModel { get; set; }

        [DisplayName("Suma")]
        public decimal Total { get; set; }

        [Required(ErrorMessage = "Imię jest wymagane")]
        [DisplayName("Imię")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Nazwisko jest wymagane")]
        [DisplayName("Nazwisko")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Numer telefonu jest wymagany - ulatwi nam to realizację zamówienia")]
        [DisplayName("Numer telefonu")]
        [RegularExpression("[0-9]{9}", ErrorMessage = "Numer telefonu musi mieć dokładnie 9 cyfr i nie może zawierać żadnych innych znaków.")]
        public string PhoneNumber { get; set; }

        public AddressViewModel AddressViewModel { get; set; }

    }
}