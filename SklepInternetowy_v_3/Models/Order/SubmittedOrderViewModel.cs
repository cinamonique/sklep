﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class SubmittedOrderViewModel
    {
        public List<CartItemViewModel> ItemsInOrder { get; set; }

        public DeliveryDetailsViewModel DeliveryDetails { get; set; }

        public DateTime OrderDate { get; set; }

        public decimal Total
        {
            get { return ItemsInOrder.Select(i => i.TotalPriceForItem).Sum(); }
        }
    }
}