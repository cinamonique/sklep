﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class Photos
    {
        [Key]
        public int PhotoId { get; set; }

        [StringLength(255)]
        public string PhotoName { get; set; }

        public int ProductId { get; set; }

        public virtual Product Product { get; set; }

        public Photos(string path)
        {
            PhotoName = path;
        }

        private Photos() { }
    }
}