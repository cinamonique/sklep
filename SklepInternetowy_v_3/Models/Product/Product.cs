﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(50)]
        public string ProductName { get; set; }

        [Required]
        public int? CategoryId { get; set; }

        public string ProductDescription { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [Required]
        public int ProductAmount { get; set; }

        public virtual ProductCategory ProductCategory { get; set; }
        public virtual ICollection<Photos> ProductPhotos { get; set; }
        public virtual ICollection<CartItem> Items { get; set; }
    }
}