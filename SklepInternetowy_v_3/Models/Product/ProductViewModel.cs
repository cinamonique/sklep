﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Podaj nazwę produktu")]
        [MaxLength(40, ErrorMessage = "Nazwa produktu nie może być dłuższa niż 40 znaków")]
        [MinLength(15, ErrorMessage = "Nazwa produktu musi się składać z co najmniej 15 znaków")]
        [DisplayName("Nazwa produktu")]
        [DataType(DataType.MultilineText)]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "Wybierz kategorię produktu")]
        [DisplayName("Kategoria")]
        public int? CategoryId { get; set; }

        [DisplayName("Opis")]
        [DataType(DataType.MultilineText)]
        public string ProductDescription { get; set; }

        [Required(ErrorMessage = "Podaj cenę produktu")]
        [DisplayName("Cena")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Podaj ilość produktu")]
        [DisplayName("Ilość")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Ilość produktów musi mieć wartość co najmniej 1")]
        public int ProductAmount { get; set; }

        [DisplayName("Kategoria")]
        public string ProductCategory { get; set; }

        public virtual ICollection<string> ProductPhotos { get; set; }
    }
}