﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class ProductCategory
    {
        [Key]
        public int CategoryId { get; set; }

        [Required]
        [MinLength(4)]
        [MaxLength(30)]
        public string CategoryName { get; set; }

        [ForeignKey("ParentCategory")]
        public int? ParentCategoryId { get; set; }

        public virtual ProductCategory ParentCategory { get; set; }

        public virtual ICollection<ProductCategory> SubCategories { get; set; }

        public virtual ICollection<Product> Products { get; set; }


        public ProductCategory()
        {
        }

        public ProductCategory(string categoryName, int? parentCategoryId)
        {
            CategoryName = categoryName;
            ParentCategoryId = parentCategoryId;
        }
    }
}