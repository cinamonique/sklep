﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class ProductCategoryViewModel
    {
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Wpisz nazwę kategorii")]
        [MaxLength(30, ErrorMessage = "Kategoria nie może być dłuższa niż 30 znaków")]
        [MinLength(4, ErrorMessage = "Kategoria musi się składać z co najmniej 4 znaków")]
        [DisplayName("Nazwa kategorii")]
        public string CategoryName { get; set; }

        [DisplayName("Kategoria główna")]
        public int? ParentCategoryId { get; set; }

        [DisplayName("Kategoria główna")]
        public string ParentCategoryName { get; set; }
    }
}