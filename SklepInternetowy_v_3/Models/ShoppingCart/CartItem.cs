﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class CartItem
    {
        [Key]
        public int ItemId { get; set; }

        [Required]
        public int ProductId { get; set; }

        public string CartId { get; set; }

        [Required]
        public int Quantity { get; set; }

        public virtual Product Product { get; set; }
        public virtual ShoppingCart Cart { get; set; }

        public CartItem() { }

        public CartItem(int productId, string cartId, int quantity)
        {
            ProductId = productId;
            CartId = cartId;
            Quantity = quantity;
        }
    }
}     