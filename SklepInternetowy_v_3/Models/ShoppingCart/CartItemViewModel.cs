﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class  CartItemViewModel
    {
        // Specifies CartItemId
        public int ItemId { get; set; }

        [Required(ErrorMessage = "Wartość wymagana")]
        [DisplayName("Ilość towaru")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Wartość musi być dodatnia")]
        public int Quantity { get; set; }

        public ProductCartViewModel ProductInCart { get; set; }

        public decimal TotalPriceForItem => Quantity * ProductInCart.Price;
    }
}