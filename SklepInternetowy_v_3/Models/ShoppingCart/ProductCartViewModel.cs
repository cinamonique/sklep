﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class ProductCartViewModel
    {
        public int ProductId { get; set; }

        [DisplayName("Produkt")]
        public string ProductName { get; set; }

        [DisplayName("Cena")]
        public decimal Price { get; set; }

        public string ProductPhoto { get; set; }

    }
}