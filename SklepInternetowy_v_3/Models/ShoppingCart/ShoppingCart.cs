﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class ShoppingCart
    {
        [Key, ForeignKey("User")]
        public string CartId { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<CartItem> Items { get; set; }
    }
} 