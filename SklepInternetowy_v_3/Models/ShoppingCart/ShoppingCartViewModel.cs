﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class ShoppingCartViewModel
    {
        public string CartId { get; set; }

        public List<CartItemViewModel> Items { get; set; }

        public decimal TotalPrizesUp
        {
            get
            {
                decimal total = 0;
                foreach (CartItemViewModel item in Items)
                {
                    total += item.Quantity * item.ProductInCart.Price;
                }
                return total;
            }
        }
    }
    
}