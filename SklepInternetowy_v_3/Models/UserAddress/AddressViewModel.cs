﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SklepInternetowy_v_3.Models
{
    public class AddressViewModel
    {
        public string AddressId { get; set; }

        [Required(ErrorMessage = "Podaj nazwę ulicy lub miejscowości")]
        [StringLength(50, ErrorMessage = "{0} musi się składać z co najmniej {2} znaków.", MinimumLength = 2)]
        [DisplayName("Ulica/Miejscowość")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Podaj numer domu lub numer bloku")]
        [DisplayName("Nr domu/bloku")]
        public int? HouseNumber { get; set; }

        [DisplayName("Numer mieszkania")]
        public int? FlatNumber { get; set; }

        [Required(ErrorMessage = "Podaj kod pocztowy")]
        [RegularExpression("[0-9]{2}-[0-9]{3}", ErrorMessage = "Podaj kod pocztowy w postaci 00-000 (bez spacji)")]
        [DisplayName("Kod pocztowy")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Podaj miasto")]
        [StringLength(30, ErrorMessage = "{0} musi się składać z co najmniej {2} znaków.", MinimumLength = 2)]
        [DisplayName("Miasto")]
        public string City { get; set; }
    }
}