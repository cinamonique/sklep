﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SklepInternetowy_v_3.Startup))]
namespace SklepInternetowy_v_3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
